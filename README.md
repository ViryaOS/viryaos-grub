# viryaos-grub

## `Dockerfile.image`

```
$ cd viryaos-grub/

$ docker build --force-rm --squash --file Dockerfile.image -t viryaos-grub .
```

## `/vos_run`

Go to the directory containing `ViryaOS` tree.

```
$ docker run --rm -ti -v $(pwd):/home/builder/src \
    viryaos-grub /vos_run
```

## `Dockerfile.package`

```
$ cd viryaos-grub

$ docker build --force-rm --squash --file Dockerfile.package -t viryaos-grub-package . 
```
